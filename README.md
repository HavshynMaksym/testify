# Deploy testify

### Для начала нужно зарегистрироваться и создать сервер на сайте AWS

Для регистрации переходим на сайт [AWS](https://aws.amazon.com/ru/) и создём свой аккаунт.

Далее:
+ Создаем **Instances**, для этого заходим в **Services** и выбираем **EC2**.  
+ Переходим в **Instances _new_** -> **Launch instances**, далее в поиске находим **"Ubuntu Server 20.04 LTS (HVM), SSD Volume Type" (_Free tier eligible_)**.  
+ Выбираем **t2.micro (_Free tier eligible_)** и нажимаем **Review and Launch** -> **Launch**.  
+ В появившимся окне выбираем **create a new key pair** и даем любое имя (_например: main_), нажимаем **Download Key Pair** и сохраняем на компьютер.  
+ Далее **Launch Instances** -> **View Instances** вот и готовый **Instances** на AWS.
+ Сохраненный файл **main.pem** нужно скопировать в папку **.ssh/main.pem**, для этого нужно выполнить команду:  
```
cp <путь, где сохранен файл>/<название файла>.pem ~/.ssh/<название файла>.pem  
(пример: cp ~/Desctop/main.pem ~/.ssh/main.pem) 
```  

### Изменения в security groups, установка 80 порта на AWS
+ Выбираем свой **Instance** и переходим на вкладку **Security**, ниже будет **Security groups** нажимаем на ссылку вида: **_sg-018d9559429fd183e (launch-wizard-2)_** (у Вас она будет немного другого содержания).  
+ Нажимаем на кнопку **Edit inbound rules** и добавляем новое правило **Add rule**, далее в **Type** выбираем **Custom TCP**, в **Port range** ставим **80** и даем доступ всем пользователям,
 для этого выбираем в **Source** порт **0.0.0.0/0** и нажимаем **Save rules**.

### Подключение сервера к локальному компьютеру
Далее ограничиваем права доступа к ключу:  
```
sudo chmod 400 ~/.ssh/<servername> 
(пример:sudo chmod 400 ~/.ssh/main.pem)
```
Подключеаем локальный компьютер к серверу:  
```
ssh -i ~/.ssh/<servername> ubuntu@<ipserver> 
(пример:ssh -i ~/.ssh/main.pem ubuntu@18.219.196.60)
```  
(что бы узнать **ipserver** нужно зайти на свой **Instance** и в вкладке **Details** найти **Public IPv4 address**).

Копируем все файлы с удаленного репозитория:  
```git clone https://gitlab.com/HavshynMaksym/testify.git```

Переходим в папку **testify**:  
```cd testify```

Переключаемся на ветку **HW/deploy**:   
```git checkout HW/deploy```

### Установка докера на сервере
Следующим шагом нужно выполнить установку **Docker**, подробная информация - [тут](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru).
Для этого нужно выполнить поочередно все ниже перечисленные команды:
```
sudo apt update -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" -y
sudo apt update -y
apt-cache policy docker-ce
sudo apt install docker-ce -y
```
**НО**, перед тем как продолжить установку, нужно назначить новый пароль для **ubuntu** на нашем сервере:  
```sudo passwd ubuntu ```

```
пример:  
New password:123456789  
Retype new password:123456789
```

После добавления пароля продолжим установку докера, выполняя поочередно команды ниже:
```
sudo usermod -aG docker ${USER}
su - ${USER}
id -nG
sudo usermod -aG docker ubuntu
```
### Установка docker-compose
После установки **Docker** устанавливаем **docker-compose**, выполняя поочередно команды, подробная информация - [тут](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-ru).
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
Далее проверяем, находитесь ли Вы в папке **testify**, если нет, то выполнить команду ниже:  
```cd testify/```

### Настройка внутренних файлов
Для начала нужно скопировать файл **".env.example"** и дать ему имя **".env"**:  
```cp .env.example .env```

Зайти в редактор nano и отредактировать файл **".env"**:  
```nano .env```

В файле будут находиться следующие переменные:
```
#prod test staging dev
RUN_MODE=prod

#Django
PORT=8000
ALLOWED_HOST=xxxx:хххх
SECRET_KEY=xxxx

# Postgresql
DB_NAME=testify
DB_HOST=postgresql
DB_PASSWORD=xxxx
DB_USER=testify_admin
DB_PORT=5432

#Gunicorn
WORKERS=4
```
Вместо "хххх" нужно будет добавить свои SECRET_KEY, DB_PASSWORD и ALLOWED_HOSTS, где:  
SECRET_KEY - секретный ключ от проекта;  
DB_PASSWORD - секретный пароль от базы;  
ALLOWED_HOSTS - нужно вставить **"Public IPv4 DNS"** и **"Public IPv4 address"** ;  
("Public IPv4 DNS" можно взять с Вашого инстанса в AWS на вкладке Details)  
Так же есть возможность поменять:  
PORT - порт запросов от nginx, дефолтное значение 8000;  
DB_PORT - порт базы данных, дефолтное значение 5432;   
DB_NAME - имя базы данных;  
DB_USER - имя админа базы данных;  
DB_PORT - порт для базы данных;  
WORKERS - количество подинстансов для распределения нагрузки на сервер.

Чтобы сгенерировать SECRET_KEY, можно воспользоваться [сайтом для генирации ключей.](https://djecrety.ir/)    
(p.s. далее нам понадобяться данные с файл ".env" )

Для сохранения в редакторе **nano**, комбинация клавиш:  
```ctrl + o```

Чтобы выйти из редактора **nano**, комбинация клавиш:  
```ctrl + x```

### Работа с docker-compose
Для начала запускаем наше приложение в daemon режиме:  
```docker-compose up -d```

Тут нужно проверить, что Вы находитесь в папке **testify**.  
Далее выполнить команду collectstatic для сбора и копирования статики:  
```docker-compose exec backend python manage.py collectstatic ```

Также нужно скопировать все медиа файлы на наш сервер:  
```docker-compose exec backend cp -r media/ /var/www/testify/media```

###Настройка базы данных postgresql
Заходим в контейнер **postgresql**:  
```docker-compose exec postgresql bash```

Переходим в консоль **postgresql**:
```
su - postgres
psql
```
Настраиваем postgresql, данные должны соответствовать **".env"** файлу, который мы ранее определили:
```
CREATE DATABASE testify; 
CREATE USER testify_admin WITH PASSWORD 'xxxx';
ALTER ROLE testify_admin SET client_encoding TO 'utf8';
ALTER ROLE testify_admin SET default_transaction_isolation TO 'read committed';
ALTER ROLE testify_admin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE testify TO testify_admin;
ALTER USER testify_admin CREATEDB;
```
И последним шагом будет миграция для добавления таблиц и связей между ними в нашу базу данных:  
```sudo docker-compose exec backend python manage.py migrate ```

Вот и все, наш сайт находится на сервере и успешно работает.  
Что бы перейти на сайт, необходимо ввести в браузере **"Public IPv4 DNS"**.

p.s.  
Если Вы хотите добавить мою тестовую базу данных, то необходимо выполнитьследующую команду:  
```docker-compose exec backend python manage.py loaddata tests/fixtures/dump.json```
