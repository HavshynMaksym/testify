from accounts.models import User

from testify.models import Question, TestResult


class TestRunner:

    def __init__(self, test_result, on_next=None):
        self.test_result = test_result
        self.on_next = on_next

    def next(self, context): # noqa

        if self.test_result.state == TestResult.STATE.NEW:
            self.on_new(context)

        elif self.test_result.state == TestResult.STATE.FINISHED:
            self.on_finish(context)

        if self.on_next:
            return self.on_next(context, self.test_result)

        return None

    def on_new(self, context):
        selected_choices = context['selected_choices']
        answers = Question.objects.get(
            test=self.test_result.test,
            order_number=self.test_result.current_order_number
        ).answers.all()

        correct_choices = sum(
            answer.is_correct == choice
            for answer, choice in zip(answers, selected_choices)
        )

        points = int(correct_choices == len(answers))
        self.points = points

        self.test_result.num_correct_answer += points
        self.test_result.num_incorrect_answer += (1 - points)
        self.test_result.save()

        current_rating = self.test_result.user.rating

        new_rating = current_rating + self.points
        User.objects.filter(
            username=self.test_result.user,
        ).update(rating=new_rating)

        if self.test_result.current_order_number == self.test_result.test.questions.count():
            self.test_result.state = TestResult.STATE.FINISHED
        else:
            self.test_result.current_order_number += 1
        self.test_result.save()

    def on_finish(self, context):
        self.test_result.state = TestResult.STATE.FINISHED
        self.test_result.save()
