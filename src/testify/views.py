from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render # noqa
from django.urls import reverse
from django.views import View
from django.views.generic import DetailView, ListView

from testify.forms import AnswerFormSet
from testify.models import Question, Test, TestResult
from testify.services import TestRunner


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    template_name = 'test.html'
    context_object_name = 'tests'
    paginate_by = 10


class TestDetailView(LoginRequiredMixin, DetailView):
    model = Test
    template_name = 'details.html'
    context_object_name = 'test'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['continue_flag'] = TestResult.objects.filter(
            user=self.request.user,
            state=TestResult.STATE.NEW,
            test=self.get_object(),
        ).count()

        best_result = TestResult.objects.filter(
            user=self.request.user,
            state=TestResult.STATE.FINISHED,
        ).extra(select={
            'points': 'num_correct_answer - num_incorrect_answer',
            'duration': 'write_date - create_date'
        }, order_by=['-points', 'duration']).first()

        if best_result is not None:
            context['best_result'] = best_result.points
            context['best_user'] = best_result.user.username

        last_run = TestResult.objects.values_list('write_date').last()
        context['last_run'] = last_run[0]

        results = TestResult.objects.all().select_related('user')
        context['results'] = results

        time_start = TestResult.objects.filter(
                    user=self.request.user,
                    state=TestResult.STATE.NEW,
                    test=self.get_object()
                ).values().last()
        if time_start is not None:
            context['time_start'] = time_start['create_date']

        return context


class TestRunnerView(LoginRequiredMixin, View):

    def get(self, request, id):  # noqa
        TestResult.objects.get_or_create(
            user=request.user,
            state=TestResult.STATE.NEW,
            test=Test.objects.get(id=id),
            defaults=dict(
                num_correct_answer=0,
                num_incorrect_answer=0,
                current_order_number=1
            )
        )

    # def get(self, request, id):  # noqa
    #     current_user_tests = TestResult.objects.filter(
    #         user=request.user,
    #         state=TestResult.STATE.NEW,
    #         test=Test.objects.get(id=id),
    #     )
    #
    #     if current_user_tests.count() == 0:
    #         TestResult.objects.create(
    #             user=request.user,
    #             state=TestResult.STATE.NEW,
    #             test=Test.objects.get(id=id),
    #             num_correct_answer=0,
    #             num_incorrect_answer=0,
    #             current_order_number=1
    #         )

        return redirect(reverse('tests:next', args=(id,)))

    @staticmethod
    def on_next(context, test_result):
        result = HttpResponse(f'Unexpected state {test_result.state}!', status=500)

        if test_result.user.popup_result == 1:
            result = context['points']
            if result == 1:
                messages.success(context['request'], 'Nice! That\'s correct answer')
            else:
                messages.warning(context['request'], 'Oops! That\'s wrong answer')

        if test_result.state == TestResult.STATE.NEW:
            result = redirect(reverse(
                'tests:next',
                args=(test_result.test.id,)
            ))

        elif test_result.state == TestResult.STATE.FINISHED:
            result = render(
                request=context['request'],
                template_name='finish.html',
                context={
                    'test_result': test_result,
                }
            )

        return result


class QuestionView(LoginRequiredMixin, View):

    def get(self, request, id):  # noqa
        test_results = TestResult.objects.filter(
            user=request.user,
            state=TestResult.STATE.NEW,
            test=Test.objects.get(id=id),
        )
        if test_results.count() == 0:
            return redirect(reverse('tests:details', args=(id,)))

        test_result = test_results.first()

        question = Question.objects.get(
            test_id=test_result.test.id,
            order_number=test_result.current_order_number
        )
        answers = question.answers.all()
        form_set = AnswerFormSet(queryset=answers)

        result = render(
            request=request,
            template_name='question.html',
            context={
                'question': question,
                'form_set': form_set,
            }
        )

        return result


    def post(self, request, id, **kwargs):  # noqa
        test_results = TestResult.objects.filter(
            user=request.user,
            state=TestResult.STATE.NEW,
            test=Test.objects.get(id=id),
        )
        if test_results.count() == 0:
            return redirect(reverse('tests:details', args=(id,)))

        test_result = test_results.first()

        form_set = AnswerFormSet(data=request.POST)

        selected_choices = [
            'is_selected' in form.changed_data
            for form in form_set.forms
        ]

        test_runner = TestRunner(
            on_next=TestRunnerView.on_next,
            test_result=test_result
        )

        result = test_runner.next(
            context={
                'request': request,
                'selected_choices': selected_choices,
            }
        )

        return result
