# from accounts.models import User
#
# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.shortcuts import get_object_or_404, redirect, render # noqa
# from django.urls import reverse
# from django.views import View
# from django.views.generic import DetailView, ListView
#
# from testify.forms import AnswerFormSet
# from testify.models import Question, Test, TestResult
#
#
# class TestListView(LoginRequiredMixin, ListView):
#     model = Test
#     template_name = 'test.html'
#     context_object_name = 'tests'
#     paginate_by = 10
#
#
# class TestDetailView(LoginRequiredMixin, DetailView):
#     model = Test
#     template_name = 'details.html'
#     context_object_name = 'test'
#     pk_url_kwarg = 'id'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#
#         context['continue_flag'] = TestResult.objects.filter(
#             user=self.request.user,
#             state=TestResult.STATE.NEW,
#             test=self.get_object(),
#         ).count()
#
#         best_result = TestResult.objects.filter(
#             user=self.request.user,
#             state=TestResult.STATE.FINISHED,
#         ).extra(select={
#             'points': 'num_correct_answer - num_incorrect_answer',
#             'duration': 'write_date - create_date'
#         }, order_by=['-points', 'duration']).first()
#
#         context['best_result'] = best_result.points
#         context['best_user'] = best_result.user.username
#
#         last_run = TestResult.objects.values_list('write_date').last()
#         context['last_run'] = last_run[0]
#
#         time_start = TestResult.objects.filter(
#                     user=self.request.user,
#                     state=TestResult.STATE.NEW,
#                     test=self.get_object()
#                 ).values().last()
#         if time_start is not None:
#             context['time_start'] = time_start['create_date']
#
#         return context
#
#
# class TestRunnerView(LoginRequiredMixin, View):
#
#     def get(self, request, id):  # noqa
#         TestResult.objects.get_or_create(
#             user=request.user,
#             state=TestResult.STATE.NEW,
#             test=Test.objects.get(id=id),
#             defaults=dict(
#                 num_correct_answer=0,
#                 num_incorrect_answer=0,
#                 current_order_number=1
#             )
#         )
#
#     # def get(self, request, id):  # noqa
#     #     current_user_tests = TestResult.objects.filter(
#     #         user=request.user,
#     #         state=TestResult.STATE.NEW,
#     #         test=Test.objects.get(id=id),
#     #     )
#     #
#     #     if current_user_tests.count() == 0:
#     #         TestResult.objects.create(
#     #             user=request.user,
#     #             state=TestResult.STATE.NEW,
#     #             test=Test.objects.get(id=id),
#     #             num_correct_answer=0,
#     #             num_incorrect_answer=0,
#     #             current_order_number=1
#     #         )
#
#         return redirect(reverse('tests:next', args=(id,)))
#
#
# class QuestionView(LoginRequiredMixin, View):
#
#     def get(self, request, id):  # noqa
#         # test_result = get_object_or_404(
#         #     TestResult,
#         #     user=request.user,
#         #     state=TestResult.STATE.NEW,
#         #     test=Test.objects.get(id=id),
#         # )
#
#         test_results = TestResult.objects.filter(
#             user=request.user,
#             state=TestResult.STATE.NEW,
#             test=Test.objects.get(id=id),
#         )
#
#         if test_results.count() == 0:
#             return redirect(reverse('tests:details', args=(id,)))
#
#         test_result = test_results.first()
#
#         order_number = test_result.current_order_number
#         question = Question.objects.get(test_id=id, order_number=order_number)
#         answers = question.answers.all()
#         form_set = AnswerFormSet(queryset=answers)
#
#         return render(
#             request=request,
#             template_name='question.html',
#             context={
#                 'question': question,
#                 'form_set': form_set,
#             }
#         )
#
#     def post(self, request, id):  # noqa
#         # test_result = get_object_or_404(
#         #     TestResult,
#         #     user=request.user,
#         #     state=TestResult.STATE.NEW,
#         #     test=Test.objects.get(id=id),
#         # )
#
#         test_results = TestResult.objects.filter(
#             user=request.user,
#             state=TestResult.STATE.NEW,
#             test=Test.objects.get(id=id),
#         )
#
#         if test_results.count() == 0:
#             return redirect(reverse('tests:details', args=(id,)))
#
#         test_result = test_results.first()
#
#         order_number = test_result.current_order_number
#         question = Question.objects.get(test__id=id, order_number=order_number)
#         answers = question.answers.all()
#         form_set = AnswerFormSet(data=request.POST)
#
#         possible_choices = len(form_set.forms)
#         selected_choices = [
#             # form.cleaned_data.get('is_selected', False)
#             'is_selected' in form.changed_data
#             for form in form_set.forms
#         ]
#
#         correct_choices = sum(
#             answer.is_correct == choice
#             for answer, choice in zip(answers, selected_choices)
#         )
#
#
#         point = int(correct_choices == possible_choices)
#
#         test_result = TestResult.objects.get(
#             user=request.user,
#             test=question.test,
#             state=TestResult.STATE.NEW
#         )
#
#         test_result.num_correct_answer += point
#         test_result.num_incorrect_answer += (1 - point)
#         test_result.current_order_number += 1
#         test_result.save()
#
#         if order_number == question.test.questions.count():
#             test_result.state = TestResult.STATE.FINISHED
#             test_result.save()
#
#             current_rating = request.user.rating
#
#             new_rating = current_rating + test_result.points()
#             User.objects.filter(
#                 username=request.user,
#             ).update(rating=new_rating)
#
#             return render(
#                 request=request,
#                 template_name='finish.html',
#                 context={
#                     'test_result': test_result,
#                 }
#             )
#
#         else:
#             return redirect(reverse('tests:next', args=(id, )))
