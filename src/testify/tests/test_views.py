from django.test import Client, TestCase
from django.urls import reverse

from testify.models import Test


class TestDetailsViewTest(TestCase):
    fixtures = [
        'tests/fixtures/dump.json',
    ]

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='admin')

    def test_details(self):
        response = self.client.get(reverse('tests:details', kwargs={'id': 1}))
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.context_data.get('test'))


class TestRunnerView(TestCase):
    fixtures = [
        'tests/fixtures/dump.json',
    ]
    TEST_ID = 1

    def setUp(self):
        self.client = Client()
        self.client.login(username='admin', password='admin')

    def test_success_flow(self):
        response = self.client.get(reverse('tests:start', kwargs={'id': self.TEST_ID}))
        self.assertRedirects(response, reverse('tests:next', kwargs={'id': self.TEST_ID}))

        test = Test.objects.get(id=self.TEST_ID)

        for question in test.questions.order_by('order_number'):
            next_url = reverse('tests:next', args=(self.TEST_ID,))
            response = self.client.get(next_url)
            self.assertEqual(response.status_code, 200)

            form = question.answers.count()
            correct_answer = [answer.is_correct for answer in question.answers.all()]

            data = {
                'form-TOTAL_FORMS': f'{form}',
                'form-INITIAL_FORMS': f'{form}',
                'form-MIN_NUM_FORMS': '0',
                'form-MAX_NUM_FORMS': '1000',
            }

            for index, answer in enumerate(correct_answer):
                if answer == True: # noqa
                    data[f'form-{index}-is_selected'] = 'on'

            response = self.client.post(
                path=next_url,
                data=data,
            )

            if question.order_number < test.questions.count():
                self.assertRedirects(response, next_url)
            else:
                self.assertEqual(response.status_code, 200)

            # self.assertEqual(response, Answer.objects.filter(is_correct=1).count())
                self.assertEqual(test.test_results.last().points(), test.questions.count())

        self.assertContains(response, 'Congratulations!')
