from django.forms.models import inlineformset_factory
from django.test import TestCase

from testify.forms import AnswerAdminForm, AnswerInlineFormSet
from testify.models import Answer, Question


class AdminAnswerInlineFormSetTest(TestCase):
    fixtures = [
        'tests/fixtures/dump.json',
    ]

    def setUp(self):
        self.question = Question.objects.first()

        self.AnswerFormSet = inlineformset_factory(
            Question, Answer, AnswerAdminForm, formset=AnswerInlineFormSet)

        self.data = {
            'answers-TOTAL_FORMS': 4,
            'answers-INITIAL_FORMS': 4,
            'answers-MIN_NUM_FORMS': 0,
            'answers-MAX_NUM_FORMS': 1000,
            'answers-0-text': '1',
            'answers-0-id': '12',
            'answers-0-is_correct': None,
            'answers-1-text': '5',
            'answers-1-is_correct': None,
            'answers-1-id': '13',
            'answers-2-text': '7',
            'answers-2-is_correct': 'on',
            'answers-2-id': '14',
            'answers-3-text': '81',
            'answers-3-is_correct': None,
            'answers-3-id': '15',
        }

    def test_formset_is_valid_if_answers_number_is_in_range(self):
        formset = self.AnswerFormSet(self.data, instance=self.question)
        self.assertEqual(formset.is_valid(), True)

    def test_formset_is_invalid_if_answers_number_is_out_of_range_max(self):
        self.data['answers-TOTAL_FORMS'] = Question.ANSWER_MAX_LIMIT + 1
        formset = self.AnswerFormSet(self.data, instance=self.question)
        self.assertEqual(formset.is_valid(), False)

    def test_formset_is_invalid_if_answers_number_is_out_of_range_min(self):
        self.data['answers-TOTAL_FORMS'] = Question.ANSWER_MIN_LIMIT - 1
        formset = self.AnswerFormSet(self.data, instance=self.question)
        self.assertEqual(formset.is_valid(), False)

    def test_formset_is_invalid_if_answers_is_all_correct(self):
        self.data['answers-0-is_correct'] = 'on'
        self.data['answers-1-is_correct'] = 'on'
        self.data['answers-3-is_correct'] = 'on'
        formset = self.AnswerFormSet(self.data, instance=self.question)
        self.assertEqual(formset.is_valid(), False)

    def test_formset_is_invalid_if_answers_is_all_incorrect(self):
        self.data['answers-2-is_correct'] = None
        formset = self.AnswerFormSet(self.data, instance=self.question)
        self.assertEqual(formset.is_valid(), False)
