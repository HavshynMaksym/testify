from django import forms
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet, ModelForm, modelformset_factory

from testify.models import Answer, Question


class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = '__all__'

    def clean(self):
        pass


class AnswerAdminForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = '__all__'


class AnswerForm(ModelForm):
    is_selected = forms.BooleanField(required=False)

    class Meta:
        model = Answer
        fields = ['text', 'is_selected']


class QuestionsInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.QUESTION_MIN_LIMIT <= len(self.forms) <= self.instance.QUESTION_MAX_LIMIT):
            raise ValidationError('Quantity of questions is out of range ({}..{})'.format(
                self.instance.QUESTION_MIN_LIMIT, self.instance.QUESTION_MAX_LIMIT
            ))

        order_number = []
        for form in self.forms:
            order_number.append(form.cleaned_data['order_number'])

        for question_amount, order_number in zip(range(1, len(order_number)+1), order_number):
            if question_amount != order_number:
                raise ValidationError('incorrect order')


class AnswerInlineFormSet(BaseInlineFormSet):

    def clean(self):
        if not (self.instance.ANSWER_MIN_LIMIT <= len(self.forms) <= self.instance.ANSWER_MAX_LIMIT):
            raise ValidationError('Quantity of answers is out of range ({}..{})'.format(
                self.instance.ANSWER_MIN_LIMIT, self.instance.ANSWER_MAX_LIMIT
            ))

        num_correct_answers = sum([
            1
            for form in self.forms
            if form.cleaned_data['is_correct']
        ])

        if num_correct_answers == 0:
            raise ValidationError('At LEAST one answer must be correct!')

        if num_correct_answers == len(self.forms):
            raise ValidationError('Not allowed to select ALL answers!')


AnswerFormSet = modelformset_factory(
    model=Answer,
    form=AnswerForm,
    extra=0
)
