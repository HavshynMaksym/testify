from accounts.models import User

from django import forms
from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm, UserCreationForm
from django.forms import ModelForm, CheckboxInput, Form, fields  # noqa


class AccountCreateForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = User
        fields = ['username', 'password1', 'password2', 'email']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User
        fields = ['image', 'username', 'first_name', 'last_name', 'email', 'popup_result']
        widgets = {
            'show_result': CheckboxInput(),
        }


class AccountPasswordChangeForm(PasswordChangeForm):
    pass


class ContactUs(Form):
    subject = fields.CharField(max_length=256, empty_value='Message from Testify')
    message = fields.CharField(widget=forms.Textarea)
