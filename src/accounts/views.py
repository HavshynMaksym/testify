from accounts.forms import AccountCreateForm, AccountPasswordChangeForm, AccountUpdateForm, ContactUs
from accounts.models import User

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.core.mail import send_mail
from django.http import HttpResponseRedirect # noqa
# from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, FormView, ListView, UpdateView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Account successfully created!')

        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, f'{request.user}, hello to LMS!')
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def get_template_names(self):
        result = super().get_template_names()
        request = self.request
        messages.info(request, 'Goodbye!')
        return result


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'profile.html'
    form_class = AccountUpdateForm
    success_url = reverse_lazy('core:index')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.info(request, 'Profile successfully updated')
        return result


class AccountPasswordChangeView(PasswordChangeView):
    form_class = AccountPasswordChangeForm
    template_name = 'change-password.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Password successfully updated ')
        return result


class UserListView(ListView):
    model = User
    template_name = 'leaderboard.html'
    context_object_name = 'users'
    paginate_by = 10
    queryset = User.objects.filter(rating__gt=0).order_by('-rating')


class ContactUsView(LoginRequiredMixin, FormView):
    template_name = 'contact_us.html'
    extra_context = {'title': 'Send us a message!'}
    success_url = reverse_lazy('core:index')
    form_class = ContactUs

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            send_mail(
                subject=form.cleaned_data['subject'] + ' #' + request.user.email,
                message=form.cleaned_data['message'],
                from_email=request.user.email,
                recipient_list=[settings.EMAIL_HOST_RECIPIENT],
                fail_silently=False,
            )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    # def form_valid(self, form):
    #     result = super().form_valid(form)
    #     request = self.request
    #     messages.success(request, 'Message successfully sent ')
    #     return result
