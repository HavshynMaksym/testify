from PIL import Image

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')
    rating = models.DecimalField(default=0.0, decimal_places=2, max_digits=5,
                                 validators=[MinValueValidator(0), MaxValueValidator(100)])
    popup_result = models.BooleanField(default=False)
    MAX_WIDTH_PX = 300

    def save(self, *args, **kwargs):
        image = self.image
        super().save(*args, **kwargs)
        try:
            img = Image.open(settings.MEDIA_ROOT + image.name)

            width = img.width
            height = img.height
            new_width = self.MAX_WIDTH_PX

            if width > new_width:
                percent = (new_width/width)
                new_height = int(height*percent)
                new_img = img.resize((new_width, new_height), Image.ANTIALIAS)
                new_img.save(settings.MEDIA_ROOT + image.name, 'JPEG')
        except IOError:
            pass
