
# def frange(start_number, end_number=None, step_number=1):
#     start = start_number - step_number
#     if end_number is None:
#         start = - step_number
#         end_number = start_number
#
#     elif start_number < 0 and end_number < 0:
#         if step_number < 0:
#             start = start_number - step_number
#             while start > end_number - step_number:
#                 start += step_number
#                 yield start
#         else:
#             while start > end_number-step_number and step_number < 0:
#                 start += step_number
#                 yield start
#
#     if start_number < end_number - step_number and step_number < 0:
#         return None
#
#     if step_number >= 1:
#         while start < end_number-step_number:
#             start += step_number
#             yield start
#
#     if start_number > end_number and step_number < 0:
#         while start > end_number-step_number:
#             start += step_number
#             yield start


def frange(start, stop=None, step=1):

    if stop is None:
        stop = float(start)
        start = 0

    while (step > 0 and start < stop) or (step < 0 and start > stop):

        result = start
        start += step
        yield result


for i in frange(-10, 10):
    print(i)
