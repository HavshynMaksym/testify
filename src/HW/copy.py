import hashlib # noqa
import multiprocessing # noqa
import os  # noqa
import shutil # noqa
import time
from multiprocessing.pool import ThreadPool  # noqa

DST_DIR = '/home/maks/file/10GB.bin'
SRC_DIR = '/home/maks/Загрузки/10GB.bin'


class timer(): # noqa
    def __init__(self, message):
        self.message = message

    def __enter__(self):
        self.start = time.time()
        return None

    def __exit__(self, type, value, traceback): # noqa
        elapsed_time = (time.time() - self.start)
        print(self.message.format(elapsed_time))


# def calc_checksum(filepath):
#     CHUNK_SIZE = 4096
#     with open(filepath, "rb") as f:
#         file_hash = hashlib.md5()
#         chunk = f.read(CHUNK_SIZE)
#         while chunk:
#             file_hash.update(chunk)
#             chunk = f.read(CHUNK_SIZE)
#     return file_hash.hexdigest()


def generate_segments(file_size, segment_size):
    segments = []
    start = 0
    remainder = file_size
    while remainder > 0:
        offset = min(remainder, segment_size)
        remainder -= offset
        segments.append((start, start + offset))
        start += offset
    return segments


def copy_segment(filepath, dest_filepath, segment):
    # workers = 8
    CHUNK_SIZE = 4096
    with open(filepath, 'rb') as rf:
        with open(dest_filepath, 'wb+') as wf:
            rf.seek(segment[0])
            wf.seek(segment[0])
            total_size = segment[1] - segment[0]
            chunk = rf.read(min(total_size, CHUNK_SIZE))
            while chunk and total_size > 0:
                # with ThreadPool(workers) as pool:
                #     pool.map_async(wf.write, chunk)
                wf.write(chunk)
                total_size -= len(chunk)
                chunk = rf.read(min(total_size, CHUNK_SIZE))


def main():
    segments = generate_segments(
        file_size=os.path.getsize(SRC_DIR),
        segment_size=10 * 1024 * 1024
    )

    # with timer('Single-threaded version elapsed: {}s'):
    #     for segment in segments:
    #         copy_segment(
    #             filepath=SRC_DIR,
    #             dest_filepath=DST_DIR,
    #             segment=segment
    #         )

    workers = 8
    with timer('Single-threaded version elapsed: {}s'):
        for segment in segments:
            with ThreadPool(workers) as pool:
                pool.apply_async(copy_segment, args=(SRC_DIR, DST_DIR, segment))
                pool.apply_async(copy_segment, args=(SRC_DIR, DST_DIR, segment))
                pool.close()
                pool.join()

    # workers = 8
    # with timer('Single-threaded version elapsed: {}s'):
    #     for segment in segments:
    #         arr = (SRC_DIR, DST_DIR, segment)
    #         with ThreadPool(workers) as pool:
    #             pool.map(copy_segment, *arr)


main()

# if calc_checksum(DST_DIR) == calc_checksum(SRC_DIR):
#     print('ok')
# else:
#     print('no')


print(os.path.getsize(DST_DIR)-os.path.getsize(SRC_DIR))

'''
Итераторы - это объекты, у которых есть метод __iter__и __next__ и возвращают саму себя
Генераторы - это функции, которые являются подтипом итераторов, они имеют yield выражение, возвращают итерируемый обьект
'''
