#
# class frange:
#     def __next__(self):
#
#         self.start_number += self.step_number
#
#         if self.start_number < 0 and self.end_number < 0:
#             if self.step_number > 0:
#                 if self.start_number == self.end_number:
#                     raise StopIteration
#
#             elif self.start_number < self.end_number and self.step_number < 0:
#                 raise StopIteration
#
#         if self.start_number == self.end_number:
#             raise StopIteration
#
#         if self.step_number == 1:
#             if self.start_number > self.end_number - self.step_number:
#                 raise StopIteration
#
#         if self.step_number > 1:
#             if self.start_number > self.end_number:
#                 raise StopIteration
#
#         if self.end_number > self.start_number and self.step_number < 0:
#             raise StopIteration
#
#         return self.start_number
#
#     def __init__(self, start_number, end_number=None, step_number=1):
#
#         self.start_number = start_number - step_number
#         self.end_number = end_number
#         self.step_number = step_number
#
#         if self.end_number is None:
#             self.end_number = self.start_number+1
#
#             self.start_number = - self.step_number
#
#     def __iter__(self):
#         return self


class frange:
    def __init__(self, start, stop=None, step=1):
        self.start = start
        self.stop = stop
        self.step = step
        if self.stop is None:
            self.stop = float(self.start)
            self.start = 0

    def __iter__(self):
        return self

    def __next__(self):
        if (self.step > 0 and self.start >= self.stop) or \
           (self.step < 0 and self.start <= self.stop):
            raise StopIteration
        result = self.start
        self.start += self.step
        return result


for i in frange(-2, 2):
    print(i)
