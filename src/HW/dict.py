# Сам словарь не имеет возможности на изминение ключей.
# Но, можно проитерироваться по словарю и удалить старый ключь и добавить новый.
#
# dictionary[new_key] = dictionary[old_key]
# del dictionary[old_key]


numbers = {'one': 1, 'two': 2, 'three': 3}
for key in list(numbers.keys()):
    if key == 'one':
        numbers['four'] = numbers['one']
        del numbers[key]

print(numbers)
