FROM python:3.8

RUN apt-get update
RUN apt-get install git

RUN mkdir -p /srv/project/commands
RUN mkdir -p /srv/project/src
WORKDIR /srv/project/

COPY ./src ./src
COPY ./commands/ ./commands/
COPY ./requirements.txt ./
COPY ./.flake8 ./

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /srv/project/src

CMD ["bash"]

